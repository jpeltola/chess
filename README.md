Chess project is made with Unreal Engine Blueprint and C++. The point of this project is to learn Unreal Engine online multiplayer systems. Project is made with help of Youtube channel Reids Channel.

The projects menu system is made with c++/Blueprint. The c++ code of the menu system is made such that the menu system can be used in any game, that needs Hosting and client joining features. The gamemode object should implement the IMenuInterface.

Engine version 4.25.4

To play the game, 2 standalone games are needed (on the editor, or in built version). The two game instances should be run on same local network. Other player can Host the game and other player can Join by writing the IP address of the host.

Controls:
<ul>
    <li> Esc for ingame menu </li>
    <li> Left mouse for selecting a tile or piece </li>
    <li> Hold Right mouse to rotate camera </li>
    <li> Mouse wheel to zoom camera </li>
</ul>

