// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ChessGamePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API AChessGamePlayerController : public APlayerController
{
	GENERATED_BODY()
	
};
