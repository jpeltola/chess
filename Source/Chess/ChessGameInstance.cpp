// Fill out your copyright notice in the Description page of Project Settings.


#include "ChessGameInstance.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "Widget/MenuWidget.h"
#include "Widget/EndGameMenu.h"

UChessGameInstance::UChessGameInstance(const FObjectInitializer& ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(TEXT("/Game/Blueprints/Widgets/WBP_MainMenu"));
	if (!ensure(MenuBPClass.Class!= nullptr)) return;
	MenuClass = MenuBPClass.Class;

	ConstructorHelpers::FClassFinder<UUserWidget> IngameMenuBPClass(TEXT("/Game/Blueprints/Widgets/WBP_IngameMenu"));
	if (!ensure(IngameMenuBPClass.Class != nullptr)) return;
	IngameMenuClass = IngameMenuBPClass.Class;

	ConstructorHelpers::FClassFinder<UUserWidget> EndgameMenuBPClass(TEXT("/Game/Blueprints/Widgets/WBP_EndGameMenu"));
	if (!ensure(EndgameMenuBPClass.Class != nullptr)) return;
	EndgameMenuClass = EndgameMenuBPClass.Class;
}



void UChessGameInstance::Host()
{
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;
	World->ServerTravel("/Game/ChessLevel?listen");
}


void UChessGameInstance::Join(const FString& Address)
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;
	PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
}

void UChessGameInstance::Disconnect()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;
	PlayerController->ClientTravel("/Game/MainMenu", ETravelType::TRAVEL_Absolute);
}


void UChessGameInstance::LoadMenu()
{
	if (!ensure(MenuClass != nullptr)) return;
	UMenuWidget* MenuWidget = CreateWidget<UMenuWidget>(this, MenuClass);

	if (!ensure(MenuWidget != nullptr)) return;
	MenuWidget->SetMenuInterface(this);
	MenuWidget->SetUp();
}

void UChessGameInstance::LoadIngameMenu()
{
	if (!ensure(IngameMenuClass != nullptr)) return;
	UMenuWidget* MenuWidget = CreateWidget<UMenuWidget>(this, IngameMenuClass);

	if (!ensure(MenuWidget != nullptr)) return;
	MenuWidget->SetMenuInterface(this);
	MenuWidget->SetUp();
}

void UChessGameInstance::LoadEndgameMenu(const FString& Winner)
{
	if (!ensure(EndgameMenuClass != nullptr)) return;
	UEndGameMenu* MenuWidget = CreateWidget<UEndGameMenu>(this, EndgameMenuClass);

	if (!ensure(MenuWidget != nullptr)) return;
	MenuWidget->SetMenuInterface(this);
	MenuWidget->SetUp();
	MenuWidget->SetText(Winner);
	

}
