// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "EndGameMenu.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API UEndGameMenu : public UMenuWidget
{
	GENERATED_BODY()
public:
	virtual bool Initialize();
	void SetText(const FString& Text);
private:

	UPROPERTY(meta = (BindWidget))
	class UButton* LobbyButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* RematchButton;
	
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* WinnerText;

	UFUNCTION()
	void ToLobby();

	UFUNCTION()
	void Rematch();

};
