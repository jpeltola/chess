// Fill out your copyright notice in the Description page of Project Settings.


#include "EndGameMenu.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "MenuInterface.h"

bool UEndGameMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(LobbyButton != nullptr))return false;
	LobbyButton->OnClicked.AddDynamic(this, &UEndGameMenu::ToLobby);

	if (!ensure(RematchButton != nullptr))return false;
	RematchButton->OnClicked.AddDynamic(this, &UEndGameMenu::Rematch);

	return true;
}

void UEndGameMenu::ToLobby()
{
	if (MenuInterface != nullptr)
	{
		TearDown();
		MenuInterface->Disconnect();
	}
}

void UEndGameMenu::Rematch()

{	
	TearDown();
	if (MenuInterface != nullptr) 
	{
		if(GetWorld()->GetFirstPlayerController()->HasAuthority())
		{
			MenuInterface->Host();
		}
			
	}
	
}

void UEndGameMenu::SetText(const FString& Text)
{
	WinnerText->SetText(FText::FromString(Text));
}