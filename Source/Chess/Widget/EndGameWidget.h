// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MenuWidget.h"
#include "CoreMinimal.h"

/**
 * 
 */
class CHESS_API EndGameWidget: UMenuWidget
{
public:
	EndGameWidget();
	~EndGameWidget();
};
