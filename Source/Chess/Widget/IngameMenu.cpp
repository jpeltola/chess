// Fill out your copyright notice in the Description page of Project Settings.


#include "IngameMenu.h"
#include "Components/Button.h"
#include "MenuInterface.h"

bool UIngameMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success)return false;

	if (!ensure(CancelButton != nullptr))return false;
	CancelButton->OnClicked.AddDynamic(this, &UIngameMenu::CancelPressed);

	if (!ensure(QuitButton != nullptr))return false;
	QuitButton->OnClicked.AddDynamic(this, &UIngameMenu::QuitPressed);

	return true;
}

void UIngameMenu::CancelPressed() 
{
	TearDown();
}

void UIngameMenu::QuitPressed()
{
	if (MenuInterface != nullptr)
	{
		TearDown();
		MenuInterface->Disconnect();
	}
}