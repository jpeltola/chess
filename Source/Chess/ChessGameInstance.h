// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Widget/MenuInterface.h"
#include "ChessGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API UChessGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()

public:
	UChessGameInstance(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(Exec)
	void Host() override;

	UFUNCTION(Exec)
	void Join(const FString& Address) override;

	UFUNCTION(BlueprintCallable)
	void Disconnect() override;

	UFUNCTION(BlueprintCallable)
	void LoadMenu();

	UFUNCTION(BlueprintCallable)
	void LoadIngameMenu();

	UFUNCTION(BlueprintCallable)
	void LoadEndgameMenu(const FString& Winner);

	TSubclassOf<class UUserWidget> MenuClass;
	TSubclassOf<class UUserWidget> IngameMenuClass;
	TSubclassOf<class UUserWidget> EndgameMenuClass;
};
